# Piwik Analytics Plugin for Datawrapper

This plugin adds a Piwik tracking code to all pages on Datawrapper for analyzing users behavior on Datawrapper.

To use this plugin you have to add the following configuration in your ``config.yaml`` under ``plugins``:

```yaml
plugins:

  analytics-piwik:
    host: piwik.example.com
    idSite: 1

```